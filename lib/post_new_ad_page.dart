import 'package:flutter/material.dart';

class PostNewAdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade900,
        title: Row(
          children: [
            Icon(
              Icons.arrow_back_sharp,
              color: Colors.white,
              size: 22,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'Post New Ad',
              style: TextStyle(
                color: Colors.white,
                fontSize: 25,
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(

        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 50, bottom: 70),
                child: Image.asset(
                  'assets/images/bg_1.jpg',
                ),
              ),
              Text(
                'Successfully Posted',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                'Your ad has been published and is now visible to other users.',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 17,
                ),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 15),
                child: Divider(
                  color: Colors.grey.shade300,
                  height: 1,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Ad ID Number',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                    ),
                  ),
                  Text(
                    '123456789',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 25, right: 50),
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.shade300,
                    ),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                      topRight: Radius.circular(15),
                    )),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Buy',
                      style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          '\$100000000000.00',
                          style: TextStyle(
                            fontSize: 27,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.ac_unit,
                          color: Colors.amber,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      'I have \$500 USD in Turkey - Cash/Bank\nTransfer/Paytm',
                      style: TextStyle(fontSize: 17),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15, bottom: 15),
                      child: Divider(
                        color: Colors.grey.shade300,
                        height: 1,
                      ),
                    ),
                    Text(
                      'I Want Euro in Germany - Cash/Bank\nTransfer/Paytm',
                      style: TextStyle(fontSize: 17),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                        bottom: 10,
                      ),
                      child: Text(
                        'May 29,2023 9:56 AM',
                        style: TextStyle(
                            color: Colors.grey.shade400, fontSize: 16),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                            left: 12,
                            right: 12,
                            top: 5,
                            bottom: 5,
                          ),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(30)),
                          child: Row(
                            children: [
                              Icon(
                                size: 17,
                                Icons.call,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Text('Call',
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 17)),
                            ],
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 7, right: 7),
                            padding: EdgeInsets.only(
                              left: 12,
                              right: 12,
                              top: 5,
                              bottom: 5,
                            ),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(30)),
                            child: Row(
                              children: [
                                Icon(Icons.mail, size: 17, color: Colors.grey),
                                SizedBox(
                                  width: 12,
                                ),
                                Text('Chat',
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 17)),
                              ],
                            )),
                        Container(
                          padding: EdgeInsets.only(
                            left: 12,
                            right: 12,
                            top: 5,
                            bottom: 5,
                          ),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(30)),
                          child: Row(
                            children: [
                              Icon(Icons.share, size: 17, color: Colors.grey),
                              SizedBox(
                                width: 12,
                              ),
                              Text('Share',
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 17)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.blue.shade900,
                  borderRadius: BorderRadius.circular(
                    7,
                  ),
                ),
                margin: EdgeInsets.only(
                  top: 20,
                ),
                padding: EdgeInsets.all(
                  7,
                ),
                width: MediaQuery.of(context).size.width,
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    'Go Back',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 27,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
