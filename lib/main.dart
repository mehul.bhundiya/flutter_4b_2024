import 'package:flutter/material.dart';
import 'package:flutter_4b/my_database.dart';
import 'package:flutter_4b/post_new_ad_page.dart';
import 'package:flutter_4b/pre_login_screen.dart';
import 'package:flutter_4b/user_list_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bottom_nav_page.dart';
import 'image_process_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
        primaryColor: Colors.blue,
        shadowColor: Colors.black45,
      ),
      home: ImageProcessPage(),

      // FutureBuilder(
      //   future: MyDataBase().initDatabase(),
      //   builder: (context, snapshot) {
      //     if (snapshot.hasData) {
      //       if (snapshot.data != null) {
      //         return FutureBuilder(
      //           future: SharedPreferences.getInstance(),
      //           builder: (context, snapshot1) {
      //             if (snapshot1.data != null) {
      //               if (snapshot1.data!.getBool('IsUserLogin') != null &&
      //                   snapshot1.data!.getBool('IsUserLogin')!) {
      //                 return UserListPage();
      //               } else {
      //                 return PreLoginScreenPage();
      //               }
      //             } else {
      //               return Center(
      //                 child: Text('ERROR'),
      //               );
      //             }
      //           },
      //         );
      //       } else {
      //         return Center(
      //           child: Text(snapshot.error.toString()),
      //         );
      //       }
      //     } else {
      //       return CircularProgressIndicator();
      //     }
      //   },
      // ),
    );
  }
}
