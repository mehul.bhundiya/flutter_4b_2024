import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_4b/UserModel.dart';
import 'package:flutter_4b/api_executor.dart';
import 'package:flutter_4b/my_database.dart';
import 'package:flutter_4b/user_entery_page.dart';
import 'package:sqflite/sqflite.dart';
import 'package:http/http.dart' as http;

class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  TextEditingController filterController = TextEditingController();
  MyDataBase db = MyDataBase();

  List<UserModel> userList = [];
  ApiExecutor api = ApiExecutor();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'User List',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              child: Icon(
                Icons.add,
                size: 25,
                color: Colors.white,
              ),
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return UserEntryPage(
                        userDetail: null,
                      );
                    },
                  ),
                ).then((value) {
                  if (value) {
                    userList.clear();
                    setState(() {});
                  }
                });
              },
            ),
          ),
        ],
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: FutureBuilder(
        future: api.getUserListFromApi(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            userList.clear();
            if (userList.isEmpty) {
              userList.addAll(snapshot.data!);
            }
            if (userList.isNotEmpty) {
              return ListView.builder(
                itemBuilder: (context, index) {
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              userList[index].name.toString(),
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) {
                                  return UserEntryPage(
                                    userDetail: userList[index],
                                  );
                                },
                              )).then((value) {
                                if (value) {
                                  userList.clear();
                                  setState(() {});
                                }
                              });
                            },
                            icon: Icon(Icons.edit),
                          ),
                          IconButton(
                            onPressed: () {
                              showDeleteAlert(userId: userList[index].id);
                            },
                            icon: Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                itemCount: userList.length,
              );
            } else {
              return Center(
                child: Text('No User Found'),
              );
            }
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }

  showDeleteAlert({required userId}) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text('Alert!'),
          content: Text('Are you sure want to delete?'),
          actions: [
            TextButton(
              onPressed: () async {
                // db.deleteUserFromTblUser(userId: userId);
                await api.deleteUserDetailInApi(userId: userId);
                Navigator.pop(context);
                setState(() {});
              },
              child: Text('Yes'),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('No'),
            ),
          ],
        );
      },
    );
  }
}
