import 'dart:convert';

import 'package:flutter_4b/UserModel.dart';
import 'package:http/http.dart' as http;

class ApiExecutor {
  Future<List<UserModel>> getUserListFromApi() async {
    http.Response response = await http
        .get(Uri.parse('https://638029948efcfcedacfe0228.mockapi.io/api/user'));
    List<dynamic> userListLocal = jsonDecode(response.body.toString());
    List<UserModel> userList = [];
    for (int i = 0; i < userListLocal.length; i++) {
      userList.add(
        UserModel(
            city: userListLocal[i]['City'],
            name: userListLocal[i]['name'],
            id: userListLocal[i]['id']),
      );
    }
    print('DATA PRINTED ::: $userList');
    return userList;
  }

  Future<dynamic> insertUserDetailInApi(
      {required userName, required city}) async {
    Map<String, dynamic> map = {};
    map['name'] = userName;
    map['City'] = city;
    http.Response response = await http.post(
        Uri.parse('https://638029948efcfcedacfe0228.mockapi.io/api/user'),
        body: map);
    print('INSERT API RESPONSE ::: ${response.body.toString()}');
    return response;
  }

  Future<dynamic> updateUserDetailInApi(
      {required userName, required city, required userId}) async {
    Map<String, dynamic> map = {};
    map['name'] = userName;
    map['City'] = city;
    http.Response response = await http.put(
        Uri.parse(
            'https://638029948efcfcedacfe0228.mockapi.io/api/user/$userId'),
        body: map);
    print('UPDATE API RESPONSE ::: ${response.body.toString()}');
    return response;
  }

  Future<dynamic> deleteUserDetailInApi({required userId}) async {
    http.Response response = await http.delete(Uri.parse(
        'https://638029948efcfcedacfe0228.mockapi.io/api/user/$userId'));
    print('UPDATE API RESPONSE ::: ${response.body.toString()}');
    return response;
  }
}
