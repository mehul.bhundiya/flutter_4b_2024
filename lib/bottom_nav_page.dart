import 'package:flutter/material.dart';
import 'package:flutter_4b/pre_login_screen.dart';
import 'package:flutter_4b/screen_division.dart';
import 'package:flutter_4b/splash_screen_page.dart';
import 'package:flutter_4b/user_list_page.dart';

class BottomNavPage extends StatefulWidget {
  @override
  State<BottomNavPage> createState() => _BottomNavPageState();
}

class _BottomNavPageState extends State<BottomNavPage> {
  int indexValue = 1;

  List<Widget> pages = [
    PreLoginScreenPage(),
    ScreenDivisionPage(),
    SplashScreenPage(),
    UserListPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      initialIndex: indexValue,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Tabbar Demo',
          ),
        ),
        drawer: Drawer(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DrawerHeader(
                decoration: BoxDecoration(color: Colors.red),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Mehul',
                    ),
                    Text(
                      'mehul.bhundiya@darshan.ac.in',
                    ),
                  ],
                ),
              ),
              getDrawerItem('Home'),
              getDrawerItem('User List'),
              getDrawerItem('Add User'),
              getDrawerItem('About Us'),
              getDrawerItem('Contact Us'),
              getDrawerItem('Logout'),
            ],
          ),
        ),
        body: Column(
          children: [
            TabBar(
              onTap: (value) {
                indexValue = value;
              },
              indicatorColor: Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorWeight: 5,
              indicator: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(
                  20,
                ),
              ),
              tabs: [
                Icon(
                  Icons.abc,
                ),
                Icon(
                  Icons.abc,
                ),
                Icon(
                  Icons.abc,
                ),
                Icon(
                  Icons.abc,
                ),
              ],
            ),
            Expanded(child: TabBarView(children: pages)),
          ],
        ),
        // bottomNavigationBar: BottomNavigationBar(
        //   items: [
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.list),
        //       label: 'PRE LOGIN',
        //     ),
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.grid_3x3),
        //       label: 'Screen Division',
        //     ),
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.grid_3x3),
        //       label: 'Screen Division',
        //     ),
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.grid_3x3),
        //       label: 'Screen Division',
        //     ),
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.grid_3x3),
        //       label: 'Screen Division',
        //     ),
        //   ],
        //   selectedItemColor: Colors.red,
        //   unselectedItemColor: Colors.yellow,
        //   showUnselectedLabels: true,
        //   unselectedLabelStyle: TextStyle(color: Colors.yellow),
        //   currentIndex: indexValue,
        //   onTap: (value) {
        //     setState(() {
        //       indexValue = value;
        //     });
        //   },
        // ),
      ),
    );
  }

  Widget getDrawerItem(String title) {
    return Column(
      children: [
        ListTile(
          leading: Text(
            title,
            style: TextStyle(fontSize: 15),
          ),
          trailing: Icon(
            color: Colors.grey.shade400,
            Icons.arrow_forward_ios,
            size: 15,
          ),
        ),
        Divider(
          color: Colors.grey.shade300,
          height: 0.5,
        )
      ],
    );
  }
}
