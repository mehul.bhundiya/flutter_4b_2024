import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageProcessPage extends StatefulWidget {
  @override
  State<ImageProcessPage> createState() => _ImageProcessPageState();
}

class _ImageProcessPageState extends State<ImageProcessPage> {
  ImagePicker picker = ImagePicker();

  List<File> localImageFiles = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            ElevatedButton(
              onPressed: () async {
                XFile? file =
                    await picker.pickImage(source: ImageSource.camera);
                // Map<String,dynamic> map = {};
                // map['file'] = File(file!.path);
                // map['fileName'] =;
                localImageFiles.add(File(file!.path));
                setState(() {});
              },
              child: Text('Capture Image'),
            ),
            Expanded(
              child: localImageFiles.length > 0
                  ? ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Container(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Image.file(
                                  localImageFiles[index],
                                  height: 200,
                                  width: 80,
                                  fit: BoxFit.cover,
                                ),
                                Text(localImageFiles[index]
                                    .path
                                    .split('/')
                                    .last),
                              ],
                            ),
                          ),
                        );
                      },
                      itemCount: localImageFiles.length,
                    )
                  : Card(
                      child: Icon(Icons.add),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
