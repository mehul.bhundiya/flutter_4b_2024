class A {
  int a = 10;
  static A? a1;

  static A getInstance() {
    return a1 ??= A();
  }
}

class B {
  A a = A.getInstance();
  A a1 = A.getInstance();

  void displayData() {
    print('VALUE OF A : ${a.a}');
    print('VALUE OF A1 : ${a1.a}');
    a.a = 50;
    print('VALUE OF A : ${a.a}');
    print('VALUE OF A1 : ${a1.a}');
    a1.a = 20;
    print('VALUE OF A : ${a.a}');
    print('VALUE OF A1 : ${a1.a}');
  }
}

void main() {
  B b = B();
  b.displayData();

  A a = A.getInstance();
  print('VALUE OF A INSIDE MAIN : ${a.a}');
}
