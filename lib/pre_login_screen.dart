import 'package:flutter/material.dart';
import 'package:flutter_4b/user_list_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreLoginScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset(
                  'assets/images/bg_1.jpg',
                  fit: BoxFit.cover,
                ),
                Container(color: Colors.black38),
                Column(
                  children: [
                    SizedBox(
                      height: 100,
                    ),
                    Container(
                      width: 200,
                      height: 100,
                      decoration: BoxDecoration(
                        color: Colors.orange,
                        borderRadius: BorderRadius.circular(
                          10,
                        ),
                      ),
                    ),
                    Text(
                      'INDIA\'S\nMOST TRUSTED MATRIMONY BRAND',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      child: Text(
                        'THIS IS NEW YEAR HAPPY NEW YEAR',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Row(children: [
            Expanded(
              child: InkWell(
                onTap: () async {
                  SharedPreferences pref =
                      await SharedPreferences.getInstance();
                  await pref.setBool('IsUserLogin', true);
                  await pref.setString('UserName', 'Mehul');
                  await pref.setString('Password', 'ABC');
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                      builder: (context) {
                        return UserListPage();
                      },
                    ),
                  );
                },
                child: Container(
                  padding: EdgeInsets.all(30),
                  color: Colors.green,
                  child: Center(
                    child: Text(
                      'LOGIN',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(30),
                color: Colors.black,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'SIGN UP',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.arrow_right_alt_sharp,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            ),
          ]),
        ],
      ),
    );
  }
}
