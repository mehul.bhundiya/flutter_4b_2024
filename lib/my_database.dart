import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MyDataBase {
  Future<Database> initDatabase() async {
    return await openDatabase(
      join(await getDatabasesPath(), 'MyDatabase.db'),
      version: 2,
      onCreate: (db, version) {
        db.execute(
            'CREATE TABLE Tbl_User(UserId INTEGER PRIMARY KEY AUTOINCREMENT, UserName TEXT)');
      },
      onUpgrade: (db, oldVersion, newVersion) {
        db.execute(
            'CREATE TABLE Tbl_User(UserId INTEGER PRIMARY KEY AUTOINCREMENT, UserName TEXT)');
      },
    );
  }

  Future<int> insertUserDetailInTblUser({required userName}) async {
    Database db = await initDatabase();
    Map<String, dynamic> map = {};
    map['UserName'] = userName;
    int pk = await db.insert('Tbl_User', map);
    return pk;
  }

  Future<int> updateUserDetailInTblUser({required userName,required userId}) async {
    Database db =await initDatabase();
    Map<String,dynamic> map = {};
    map['UserName'] = userName;
    int pk = await db.update('Tbl_User', map,where: 'UserId = ?', whereArgs: [userId]);
    return pk;
  }

  Future<List<Map<String, dynamic>>> getUserListFromTblUser() async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> userList =
        await db.rawQuery('SELECT * FROM Tbl_User');
    return userList;
  }

  Future<int> deleteUserFromTblUser({required userId}) async {
    Database db = await initDatabase();
    int pk =
        await db.delete('Tbl_User', where: 'UserId = ?', whereArgs: [userId]);
    return pk;
  }
}
