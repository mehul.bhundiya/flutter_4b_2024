import 'dart:convert';

/// name : "hsdgs"
/// Dob : "1943-08-09T22:46:41.451Z"
/// City : "xzcz"
/// IsFavorite : false
/// id : "32"

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    String? name,
    String? dob,
    String? city,
    bool? isFavorite,
    String? id,
  }) {
    _name = name;
    _dob = dob;
    _city = city;
    _isFavorite = isFavorite;
    _id = id;
  }

  UserModel.fromJson(dynamic json) {
    _name = json['name'];
    _dob = json['Dob'];
    _city = json['City'];
    _isFavorite = json['IsFavorite'];
    _id = json['id'];
  }

  String? _name;
  String? _dob;
  String? _city;
  bool? _isFavorite;
  String? _id;

  UserModel copyWith({
    String? name,
    String? dob,
    String? city,
    bool? isFavorite,
    String? id,
  }) =>
      UserModel(
        name: name ?? _name,
        dob: dob ?? _dob,
        city: city ?? _city,
        isFavorite: isFavorite ?? _isFavorite,
        id: id ?? _id,
      );

  String? get name => _name;

  String? get dob => _dob;

  String? get city => _city;

  bool? get isFavorite => _isFavorite;

  String? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['Dob'] = _dob;
    map['City'] = _city;
    map['IsFavorite'] = _isFavorite;
    map['id'] = _id;
    return map;
  }
}
